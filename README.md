# high_school_le_corbusier

## Context

This repository aims to create a new design for [this](http://www.lyc-lecorbusier-poissy.ac-versailles.fr/) high school site using Bootstrap.

## Content

Each `html` file is a page of the website.

This repository architecture is made according to the website map. That means:\
Website pages depending on their place in the navbar are contained in their related folder.

Except the following website pages which are stored in the root of the project:
1. `index.html`: the homepage has no place in the navbar
1. `contact.html`: this page is in the navbar and the footer
1. `transport_to_school.html`: this page is in the navbar and the footer
1. `legal_notice.html`: this page has no place in the navbar
1. `website_map.html`: this page has no place in the navbar

## Usage

Launch `index.html` in a browser.

## What could be improved

1. import jQuery, Popper and Bootstrap JS from the doynloaded folder instead of using a CDN
1. `index.html` and `news.html`: use templating to display actuality and need access to the database to display actuality.
1. `contact.html`: Submit button does nothing
1. `results.html`: Need data to display

## Sources

About Bootstrap:
1. [Openclassrooms course](https://openclassrooms.com/fr/courses/6391096-creez-des-sites-web-responsive-avec-bootstrap-4)
